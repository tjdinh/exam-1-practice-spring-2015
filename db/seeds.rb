# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Address.delete_all
Owner.delete_all
Car.delete_all

Address.create(street: "1313 Mockingbird Lane",state: "Texas", city: "Houston")
Address.create(street: "1000 Mason Road",state: "Texas", city: "Houston")

Owner.create(name: "Harry", address: Address.first)
Owner.create(name: "Sally", address: Address.first)
Owner.create(name: "Bill", address: Address.last)

Car.create(make: "Ford",model: "Mustang",vin_number: "AAA1234", owner: Owner.first)
Car.create(make: "Ford",model: "Fairlane",vin_number: "BAA1234",owner: Owner.first)
Car.create(make: "Chevy",model: "Corvette",vin_number: "CAA1234",owner: Owner.last)
Car.create(make: "Mini",model: "Cooper",vin_number: "DAA1234",owner: Owner.last)

